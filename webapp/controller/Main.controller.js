sap.ui.define([
	"com/ey/tax/mdmpoc/controller/BaseController",
	"com/ey/tax/mdmpoc/model/formatter"
], function (BaseController, Formatter) {
	"use strict";

	return BaseController.extend("com.ey.tax.mdmpoc.controller.Main", {

		formatter: Formatter,

		onInit: function () {
			// Add style according to device
			this.getView().addStyleClass(this.getOwnerComponent().getContentDensityClass());
			//Inital data in ViewModel
			// var oViewModel = this.getView().getModel("view");
			// oViewModel.setProperty("/data", "test");
		},

		onAfterRendering: function () {
			var that = this;
			var oViewModel = that.getView().getModel("view");
			oViewModel.setProperty("/HasValues", false);
			oViewModel.setProperty("/NumberOfUnmaintainedFields", 0);
			oViewModel.setProperty("/HasEditModus", false);
		},

		onEdit: function () {
			var that = this;
			var oViewModel = that.getView().getModel("view");
			oViewModel.setProperty("/HasEditModus", true);
		},

		onSave: function () {
			var that = this;
			var oViewModel = that.getView().getModel("view");
			oViewModel.setProperty("/HasEditModus", false);
		},

		onSearch: function (oEvent) {
			//Test 1010 19.01.22
			var that = this;
			var oModel = that.getView().getModel();
			var oViewModel = this.getView().getModel("view");
			var aFilters = oEvent.getParameter("selectionSet");
			if (aFilters) {
				var sCompanyCode = aFilters[0].getValue();
				var dDeadline = aFilters[1].getDateValue();
				if (dDeadline && sCompanyCode) {
					var dUTCDate = new Date(dDeadline.getTime() - dDeadline.getTimezoneOffset() * 60 * 1000);
					var sObjectPath = oModel.createKey("/CompanySet", {
						CompanyCode: sCompanyCode,
						Deadline: dUTCDate
					});
					that.getView().bindElement({
						path: sObjectPath,
						parameters: {
							expand: "to_shareholders,to_taxdecomposition"
						},
						events: {
							dataRequested: function () {
								// BusyIndicator.show();
							},
							dataReceived: function (e) {
								// BusyIndicator.hide();
								var oObject = e.getSource().oElementContext.getObject();
								if (oObject) {
									oViewModel.setProperty("/HasValues", true);
									var iCount = 0;
									for (var x in oObject) {
										if (!oObject[x]) {
											iCount++;
										}
									}
									that.getView().byId("dpFoundingDate").setDateValue(Formatter.getDateTime(oObject.FoundingDate));
									oViewModel.setProperty("/NumberOfUnmaintainedFields", iCount);
								} else {
									oViewModel.setProperty("/HasValues", false);
									oViewModel.setProperty("/NumberOfUnmaintainedFields", 0);
								}
							}
						}
					});
				} else {
					//Todo: i8n
					var msg = "Es müssen beide Pflichfelder ausgefüllt sein!";
					sap.m.MessageToast.show(msg);
				}
			} else {
				that.getView().bindElement({
					path: ""
				});
				oViewModel.setProperty("/HasValues", false);
				oViewModel.setProperty("/NumberOfUnmaintainedFields", 0);
			}
		},

		onClearFilterbar: function (oEvent) {
			var that = this;
			var oViewModel = that.getView().getModel("view");
			var aFilters = oEvent.getParameter("selectionSet");
			if (aFilters[0]) {
				aFilters[0].setValue("");
			}
			if (aFilters[1]) {
				aFilters[1].setValue("");
			}
			oViewModel.setProperty("/HasValues", false);
			oViewModel.setProperty("/NumberOfUnmaintainedFields", 0);
			that.getView().byId("dpFoundingDate").setValue("");
			oEvent.getSource().fireSearch();
		},

		onAddEstablishment: function () {
			var that = this;
			var oList = that.getView().byId("listTaxDecomposition");
			var oVbox1 = new sap.m.VBox({
				items: [new sap.m.Label({
					text: "{i18n>tax.establishment}"
				}), new sap.m.Input({
					enabled: "{view>/HasEditModus}"
				})]
			});
			var oVbox2 = new sap.m.VBox({
				items: [new sap.m.Label({
					text: "{i18n>tax.taxRate}"
				}), new sap.m.Input({
					enabled: "{view>/HasEditModus}"
				})]
			});
			var oVbox3 = new sap.m.VBox({
				items: [new sap.m.Label({
					text: "{i18n>tax.wages}"
				}), new sap.m.Input({
					enabled: "{view>/HasEditModus}"
				})]
			});
			var oItem = new sap.m.CustomListItem({
				content: new sap.m.HBox({
					justifyContent: "SpaceAround",
					items: [oVbox1, oVbox2, oVbox3]
				})
			});
			oList.addItem(oItem);
		},

		onAddShareholder: function () {
			var that = this;
			var oList = that.getView().byId("listshareholder");
			var oVbox1 = new sap.m.VBox({
				items: [new sap.m.Label({
					text: "{i18n>shareholder.id}"
				}), new sap.m.Input({
					enabled: "{view>/HasEditModus}"
				})]
			});
			var oVbox2 = new sap.m.VBox({
				items: [new sap.m.Label({
					text: "{i18n>shareholder.organBearer}"
				}), new sap.m.Switch({
					enabled: "{view>/HasEditModus}"
				})]
			});
			var oItem = new sap.m.CustomListItem({
				content: new sap.m.HBox({
					justifyContent: "SpaceAround",
					items: [oVbox1, oVbox2]
				})
			});
			oList.addItem(oItem);
		}
	});
});
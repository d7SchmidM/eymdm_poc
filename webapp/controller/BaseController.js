sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/core/routing/History",
	"com/ey/tax/mdmpoc/model/formatter",
	"sap/ui/core/message/Message",
	"sap/ui/model/Filter"
], function (Controller, History, Formatter, Message, Filter) {
	"use strict";

	return Controller.extend("com.ey.tax.mdmpoc.BaseController", {

		formatter: Formatter,
		
		getRouter: function () {
			return this.getOwnerComponent().getRouter();
		},
		
		getResourceBundle: function () {
			return this.getOwnerComponent().getModel("i18n").getResourceBundle();
		},
		
		getI18nText: function (sKey, aParams) {
			return this.getResourceBundle().getText(sKey, aParams);
		},
		
		onNavBack: function () {
			var sPreviousHash = History.getInstance().getPreviousHash(),
				oCrossAppNavigator = sap.ushell.Container.getService("CrossApplicationNavigation");

			if (sPreviousHash !== undefined || !oCrossAppNavigator.isInitialNavigation()) {
				// eslint-disable-next-line sap-no-history-manipulation
				window.history.go(-1);
			} else {
				this.getRouter().navTo("Main", {}, true);
			}
		},
		
		handleMessagePopoverPress: function (oEvent) {
			var that = this;
			var oMessagePopover = that.getView().byId("messagePopover");
			if (!oMessagePopover) {
				oMessagePopover = sap.ui.xmlfragment(that.getView().getId(), "com.ey.tax.mdmpoc.view.fragments.MessagePopover", that);
				that.getView().addDependent(oMessagePopover);
			}
			oMessagePopover.openBy(oEvent.getSource());
		},
		
		onDeleteMessages: function () {
			var that = this;
			var oMessagePopover = that.getView().byId("messagePopover");
			if (oMessagePopover) {
				sap.ui.getCore().getMessageManager().removeAllMessages();
				oMessagePopover.close();
			}
		},
		
		openMessagePopover: function () {
			var that = this;
			var oMessagePopover = that.getView().byId("messagePopover");
			if (!oMessagePopover) {
				oMessagePopover = sap.ui.xmlfragment(that.getView().getId(), "com.ey.tax.mdmpoc.view.fragments.MessagePopover", that);
				that.getView().addDependent(oMessagePopover);
			}
			oMessagePopover.openBy(that.getView().byId("messagePopoverBtn"));
		},
		
		createMessageModelItem: function (sMessage, sId, sType) {
			var oMessageProcessor = new Message.ControlMessageProcessor();
			var oMessageManager = sap.ui.getCore().getMessageManager();
			oMessageManager.registerMessageProcessor(oMessageProcessor);
			var oMessageTarget = sId !== null ? this.getView().byId(sId).getId() + "/value" : null;
			oMessageManager.addMessages(
				new Message({
					message: sMessage,
					type: sap.ui.core.MessageType[sType],
					target: oMessageTarget
				})
			);
			this.openMessagePopover();
		},
		
		createFilter: function (sPath, sOperator, sValue1, sValue2) {
			return new Filter({
				path: sPath,
				operator: sap.ui.model.FilterOperator[sOperator],
				value1: sValue1,
				value2: sValue2
			});
		},
		
		createAndFilter: function (aFilters) {
			return new Filter({
				filters: aFilters,
				and: true
			});
		},
		
		createOrFilter: function (aFilters) {
			return new Filter({
				filters: aFilters,
				and: false
			});
		},

		checkSAPUI5Version: function (sVersionToCheck) {
			var currentVersionArray = sap.ui.version.split(".");
			var checkVersionArray = sVersionToCheck.split(".");
			if (checkVersionArray.length !== 3) {
				return false;
			} else {
				for (var i = 0; i <= currentVersionArray.length; i++) {
					var check = parseInt(checkVersionArray[i],10);
					var current = parseInt(currentVersionArray[i],10);
					if (check < current) {
						return false;
					}
				}
				return true;
			}
		},
		
		getURLParameter: function (sParameterName) {
			var sURI = URI.decode(window.location.href);
			var sParameterValue = "";
			if (sURI.indexOf(sParameterName) > -1) {
				var sContainsValue = sURI.split(sParameterName + "=")[1];
				if (sContainsValue.split("&")[0].length > 0) {
					sParameterValue = sContainsValue.split("&")[0];
				} else {
					sParameterValue = sContainsValue;
				}
			}
			return sParameterValue;
		},

		checkNavigationTarget: function (sNavigationTarget) {
			var oPromise = $.Deferred();
			sap.ushell.Container.getService("NavTargetResolution").resolveHashFragment("#" + sNavigationTarget).then(function (data) {
				oPromise.resolve(true);
			}, function (reason) {
				oPromise.resolve(false);
			});
			return oPromise;
		},

		_mViewDialogs: {},
		createDialog: function (sDialogFragmentName) {
			var oDialog = this._mViewDialogs[sDialogFragmentName];
			if (!oDialog) {
				oDialog = sap.ui.xmlfragment(this.getView().getId(), sDialogFragmentName, this);
				this._mViewDialogs[sDialogFragmentName] = oDialog;
				this.getView().addDependent(oDialog);
			}
			return oDialog;
		}
		
	});
});
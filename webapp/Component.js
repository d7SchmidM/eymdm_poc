sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/ey/tax/mdmpoc/model/models",
	"com/ey/tax/mdmpoc/localService/mockserver"
], function (UIComponent, Device, models, mockserver) {
	"use strict";

	return UIComponent.extend("com.ey.tax.mdmpoc.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			// Register Message Model
			sap.ui.getCore().getMessageManager().removeAllMessages();
			this.setModel(sap.ui.getCore().getMessageManager().getMessageModel(), "message");

			mockserver.init();
			var oModel = new sap.ui.model.odata.v2.ODataModel({
				serviceUrl: "/here/goes/your/serviceurl/"
			});
			// var sPath = jQuery.sap.getModulePath("com.ey.tax.mdmpoc.annotations");
			// var sAnnoPath = sPath + "/annotations.xml";
			// oModel.addAnnotationUrl(sAnnoPath);
			this.setModel(oModel);
		},

		getContentDensityClass: function () {
			if (!this._sContentDensityClass) { //
				if (!this.getModel("device").getProperty("/isPhone") /*!Device.system.DESKTOP*/ ) {
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		}
	});
});
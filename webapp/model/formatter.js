sap.ui.define(["sap/ui/core/format/NumberFormat", "sap/ui/core/format/DateFormat"], function (NumberFormat, DateFormat) {
	"use strict";
	return {
		getDateTime: function (oDate) {
			if (!oDate) {
				return "";
			}
			var dDate = new Date(parseInt(oDate,10));
			return dDate;
		},

		formatNumber: function (sNumber, sUnit) {
			var oNumberFormat = NumberFormat.getFloatInstance({
				maxFractionDigits: 0,
				groupingEnabled: true,
				groupingSeparator: ",",
				decimalSeparator: "."
			});
			if (sNumber) {
				return oNumberFormat.parse(sNumber) + " " + sUnit;
			} else {
				return "";
			}
		}
	};
});
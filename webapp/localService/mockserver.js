sap.ui.define([
	"sap/ui/core/util/MockServer"
], function (MockServer) {
	"use strict";
	return {
		init: function () {

			var uri = '/here/goes/your/serviceurl/';
			var sPath = jQuery.sap.getModulePath("com.ey.tax.mdmpoc.localService");
			var sMetadataPath = sPath + "/metadata.xml";
			var sMockdataPath = sPath + "/mockdata";

			// create
			var oMockServer = new MockServer({
				rootUri: uri
			});
			// configure
			MockServer.config({
				autoRespond: true,
				autoRespondAfter: 500
			});
			// simulate
			oMockServer.simulate(sMetadataPath, sMockdataPath);
			// start
			oMockServer.start();

		}
	};
});